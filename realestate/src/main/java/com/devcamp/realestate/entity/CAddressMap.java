package com.devcamp.realestate.entity;
import javax.persistence.*;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "address_map")
@Data
public class CAddressMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String address;

    private Double _lat;

    private Double _lng;

    // public CAddressMap() {
    // }

    // public CAddressMap(int id, String address, Double _lat, Double _lng) {
    //     this.id = id;
    //     this.address = address;
    //     this._lat = _lat;
    //     this._lng = _lng;
    // }

    // public int getId() {
    //     return this.id;
    // }

    // public void setId(int id) {
    //     this.id = id;
    // }

    // public String getAddress() {
    //     return this.address;
    // }

    // public void setAddress(String address) {
    //     this.address = address;
    // }

    // public Double get_lat() {
    //     return this._lat;
    // }

    // public void set_lat(Double _lat) {
    //     this._lat = _lat;
    // }

    // public Double get_lng() {
    //     return this._lng;
    // }

    // public void set_lng(Double _lng) {
    //     this._lng = _lng;
    // }

    // public CAddressMap id(int id) {
    //     setId(id);
    //     return this;
    // }

    // public CAddressMap address(String address) {
    //     setAddress(address);
    //     return this;
    // }

    // public CAddressMap _lat(Double _lat) {
    //     set_lat(_lat);
    //     return this;
    // }

    // public CAddressMap _lng(Double _lng) {
    //     set_lng(_lng);
    //     return this;
    // }

}
