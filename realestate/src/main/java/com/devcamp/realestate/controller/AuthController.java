package com.devcamp.realestate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestate.entity.Token;
import com.devcamp.realestate.entity.User;
import com.devcamp.realestate.entity.UserNow;
import com.devcamp.realestate.repository.UserRepository;
import com.devcamp.realestate.security.JwtUtil;
import com.devcamp.realestate.security.UserPrincipal;
import com.devcamp.realestate.service.TokenService;
import com.devcamp.realestate.service.UserService;
import com.nimbusds.jose.proc.SecurityContext;

@RestController
// @CrossOrigin
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class AuthController {
    @Autowired
    UserService userService;

    @Autowired
    TokenService tokenService;

    @Autowired
    JwtUtil jwtUtil;

    @Autowired
    UserRepository pRepository;

    @PostMapping("/register")
    public User register(@RequestBody User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

        return userService.createUser(user);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody User user) {
        UserPrincipal userPrincipal = userService.findByUsername(user.getUsername());
        if (null == user || !new BCryptPasswordEncoder().matches(user.getPassword(), userPrincipal.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("tài khoản hoặc mật khẩu không chính xác");
        }
        Token token = new Token();
        token.setToken(jwtUtil.generateToken(userPrincipal));
        token.setTokenExpDate(jwtUtil.generateExpirationDate());
        token.setCreatedBy(userPrincipal.getUserId());
        tokenService.createToken(token);
        return ResponseEntity.ok(token.getToken());
    }

    @GetMapping("/me")
    public Long getUserId() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = ((UserPrincipal) userDetails).getUserId();
        return userId;
    }

    @GetMapping("/name")
    public Object getUserName() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserNow customers = new UserNow();
        Long userId = ((UserPrincipal) userDetails).getUserId();
        String userName = ((UserPrincipal) userDetails).getUsername();
        customers.setUserId(userId);
        customers.setUserName(userName);
        return customers;
    }

    @GetMapping("/hello")
    @PreAuthorize("hasAnyAuthority('USER_CREATE', 'USER_UPDATE')")
    /*
     * @PreAuthorize("hasRole('USER_READ') " + "|| hasRole('USER_CREATE')" +
     * "|| hasRole('USER_UPDATE')" + "|| (hasRole('USER_DELETE')")
     */

    public ResponseEntity hello() {
        return ResponseEntity.ok("hello  have USER_READ OR USER_CREATE OR USER_UPDATE oR USER_DELETE");
    }

    @GetMapping("/hello2")
    @PreAuthorize("hasAnyAuthority('USER_READ','USER_DELETE')")
    // @PreAuthorize("hasRole('USER_READ') " +
    // "|| (hasRole('USER_DELETE')")

    public ResponseEntity hello2() {
        return ResponseEntity.ok("hello 2 have USER_READ OR USER_DELETE");
    }

    @GetMapping("/hello3")
    @PreAuthorize("hasAnyRole('ADMIN', 'CUSTOMER')")
    public ResponseEntity hello3() {
        return ResponseEntity.ok("hello cho ADMIN va CUSTOMER");
    }

    @GetMapping("/hello4")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public ResponseEntity hello4() {
        return ResponseEntity.ok("hello chi cho ADMIN");
    }

    @GetMapping("/hello6")
    @PreAuthorize("hasRole('MANAGER')")
    public ResponseEntity hello6() {
        return ResponseEntity.ok("hello chi cho mANAGER");
    }

    @GetMapping("/hello7")
    @PreAuthorize("hasAnyAuthority('USER_DELETE')")
    // @PreAuthorize("hasRole('USER_READ') " +
    // "|| (hasRole('USER_DELETE')")

    public ResponseEntity hello7() {
        return ResponseEntity.ok("hello 2 have USER_DELETE");
    }

    @GetMapping("/hello5")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity hello5() {
        return ResponseEntity.ok("hello chi cho ADMIN");
    }

}
