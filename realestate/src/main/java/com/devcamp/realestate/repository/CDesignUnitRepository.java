package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CDesignUnit;

public interface CDesignUnitRepository extends JpaRepository<CDesignUnit,Integer>{
    
}
