package com.devcamp.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.entity.CStreet;

public interface CStreetRepository extends JpaRepository<CStreet,Integer>{
    @Query(value = "SELECT * FROM ward WHERE _district_id LIKE :warId%", nativeQuery = true)
	List<CStreet> findStreetLike(@Param("warId") Integer warId);
}
