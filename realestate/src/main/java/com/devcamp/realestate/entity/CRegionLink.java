package com.devcamp.realestate.entity;
import javax.persistence.*;
import javax.persistence.Table;
@Entity
@Table(name = "region_link")
public class CRegionLink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String description;

    private String photo;

    private String address;

    private Double _lat;

    private Double _lng;


    public CRegionLink() {
    }

    public CRegionLink(int id, String name, String description, String photo, String address, Double _lat, Double _lng) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.address = address;
        this._lat = _lat;
        this._lng = _lng;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double get_lat() {
        return this._lat;
    }

    public void set_lat(Double _lat) {
        this._lat = _lat;
    }

    public Double get_lng() {
        return this._lng;
    }

    public void set_lng(Double _lng) {
        this._lng = _lng;
    }

    public CRegionLink id(int id) {
        setId(id);
        return this;
    }

    public CRegionLink name(String name) {
        setName(name);
        return this;
    }

    public CRegionLink description(String description) {
        setDescription(description);
        return this;
    }

    public CRegionLink photo(String photo) {
        setPhoto(photo);
        return this;
    }

    public CRegionLink address(String address) {
        setAddress(address);
        return this;
    }

    public CRegionLink _lat(Double _lat) {
        set_lat(_lat);
        return this;
    }

    public CRegionLink _lng(Double _lng) {
        set_lng(_lng);
        return this;
    }


}
