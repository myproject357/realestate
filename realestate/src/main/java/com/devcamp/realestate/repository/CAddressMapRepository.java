package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CAddressMap;

public interface CAddressMapRepository extends JpaRepository<CAddressMap,Integer>{
    
}
