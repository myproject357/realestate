package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CLocation;

public interface CLocationRepository extends JpaRepository<CLocation,Integer>{
    
}
