package com.devcamp.realestate.entity;
import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Table(name = "utilities")
public class CUtiliti {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String description;

    private String photo;



    public CUtiliti() {
    }

    public CUtiliti(Integer id, String name, String description, String photo) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.photo = photo;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public CUtiliti id(Integer id) {
        setId(id);
        return this;
    }

    public CUtiliti name(String name) {
        setName(name);
        return this;
    }

    public CUtiliti description(String description) {
        setDescription(description);
        return this;
    }

    public CUtiliti photo(String photo) {
        setPhoto(photo);
        return this;
    }

}
