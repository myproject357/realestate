package com.devcamp.realestate.entity;
import javax.persistence.*;
import javax.persistence.Table;
@Entity
@Table(name = "province")
public class Cprovince {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String _name;

    private String _code;


    public Cprovince() {
    }

    public Cprovince(int id, String _name, String _code) {
        this.id = id;
        this._name = _name;
        this._code = _code;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String get_name() {
        return this._name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_code() {
        return this._code;
    }

    public void set_code(String _code) {
        this._code = _code;
    }

    public Cprovince id(int id) {
        setId(id);
        return this;
    }

    public Cprovince _name(String _name) {
        set_name(_name);
        return this;
    }

    public Cprovince _code(String _code) {
        set_code(_code);
        return this;
    }

}
