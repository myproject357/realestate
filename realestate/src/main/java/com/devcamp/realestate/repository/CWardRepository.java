package com.devcamp.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.entity.CWard;

public interface CWardRepository extends JpaRepository<CWard,Integer>{
    @Query(value = "SELECT * FROM ward WHERE _district_id LIKE :districtId%", nativeQuery = true)
	List<CWard> findWardLike(@Param("districtId") Integer districtId);
}
