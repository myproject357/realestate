package com.devcamp.realestate.entity;
import javax.persistence.*;
import javax.persistence.Table;
@Entity
@Table(name = "subscriptions")
public class CSubscriptions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String user;

    private String endpoint;

    private String publickey;

    private String authenticationtoken;

    private String contentencoding;


    public CSubscriptions() {
    }

    public CSubscriptions(int id, String user, String endpoint, String publickey, String authenticationtoken, String contentencoding) {
        this.id = id;
        this.user = user;
        this.endpoint = endpoint;
        this.publickey = publickey;
        this.authenticationtoken = authenticationtoken;
        this.contentencoding = contentencoding;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEndpoint() {
        return this.endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getPublickey() {
        return this.publickey;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }

    public String getAuthenticationtoken() {
        return this.authenticationtoken;
    }

    public void setAuthenticationtoken(String authenticationtoken) {
        this.authenticationtoken = authenticationtoken;
    }

    public String getContentencoding() {
        return this.contentencoding;
    }

    public void setContentencoding(String contentencoding) {
        this.contentencoding = contentencoding;
    }

    public CSubscriptions id(int id) {
        setId(id);
        return this;
    }

    public CSubscriptions user(String user) {
        setUser(user);
        return this;
    }

    public CSubscriptions endpoint(String endpoint) {
        setEndpoint(endpoint);
        return this;
    }

    public CSubscriptions publickey(String publickey) {
        setPublickey(publickey);
        return this;
    }

    public CSubscriptions authenticationtoken(String authenticationtoken) {
        setAuthenticationtoken(authenticationtoken);
        return this;
    }

    public CSubscriptions contentencoding(String contentencoding) {
        setContentencoding(contentencoding);
        return this;
    }


}
