package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CRegionLink;

public interface CRegionLinkRepository extends JpaRepository<CRegionLink,Integer>{
    
}
