package com.devcamp.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.entity.CRealEstate;

public interface CRealEstateRepository extends JpaRepository<CRealEstate,Long>{
	@Query(value = "SELECT * FROM realestate WHERE request LIKE :request%", nativeQuery = true)
	List<CRealEstate> findRealEstateByRequestLike(@Param("request") Long request);

	@Query(value = "SELECT * FROM realestate WHERE `comfirm` LIKE :comfirm%", nativeQuery = true)
	List<CRealEstate> findRealEstateByComfirm(@Param("comfirm") Integer comfirm);

	@Query(value = "SELECT * FROM `realestate` WHERE `project_id` LIKE :project_id% AND `comfirm` = 1" , nativeQuery = true)
	List<CRealEstate> findRealEstateByProject(@Param("project_id") Integer project_id);

	@Query(value = "SELECT * FROM `realestate` WHERE `create_by` LIKE :create_by%", nativeQuery = true)
	List<CRealEstate> findRealEstateByCreateBy(@Param("create_by") Integer create_by);

	@Query(value = "SELECT * FROM `realestate` WHERE `recommend` LIKE :recommend%", nativeQuery = true)
	List<CRealEstate> findRealEstateByRecommend(@Param("recommend") Integer recommend);

	// @Query(value = "SELECT e FORM realestate e WHERE e.project_id = :project_id", nativeQuery = true)
	// List<CRealEstate> findRealEstateByProjectAndComfirm(@Param("project_id") Long project_id);

// 	@Query("SELECT u FROM User u WHERE u.status = :status and u.name = :name")
// User findUserByStatusAndNameNamedParams(@Param("status") Integer status, @Param("name") String name);

// @Query(value = "SELECT * FORM realestate WHERE project_id = :project_id% AND comfirm = :comfirm%", nativeQuery = true)
// List<CRealEstate> findRealEstateByProjectAndComfirm(@Param("project_id") Integer id , @Param("comfirm") Integer comfirm);
}
