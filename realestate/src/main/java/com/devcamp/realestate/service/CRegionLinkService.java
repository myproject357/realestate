package com.devcamp.realestate.service;

import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.CRegionLink;

@Service
public class CRegionLinkService {
    public void setGetRegionLinkService(CRegionLink newEmployee , CRegionLink employee) {
        newEmployee.setAddress(employee.getAddress());
        newEmployee.setDescription(employee.getDescription());
        newEmployee.setName(employee.getName());
        newEmployee.setPhoto(employee.getPhoto());
        newEmployee.set_lat(employee.get_lat());
        newEmployee.set_lng(employee.get_lng());
    }
}
