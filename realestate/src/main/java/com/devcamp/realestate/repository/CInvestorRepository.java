package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CInvestor;

public interface CInvestorRepository extends JpaRepository<CInvestor,Integer>{
    
}
