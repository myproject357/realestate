package com.devcamp.realestate.entity;
import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Table(name = "locations")
public class CLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private float Latitude;

    private float Longitude;

    public CLocation() {
    }

    public CLocation(int id, float Latitude, float Longitude) {
        this.id = id;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getLatitude() {
        return this.Latitude;
    }

    public void setLatitude(float Latitude) {
        this.Latitude = Latitude;
    }

    public float getLongitude() {
        return this.Longitude;
    }

    public void setLongitude(float Longitude) {
        this.Longitude = Longitude;
    }

    public CLocation id(int id) {
        setId(id);
        return this;
    }

    public CLocation Latitude(float Latitude) {
        setLatitude(Latitude);
        return this;
    }

    public CLocation Longitude(float Longitude) {
        setLongitude(Longitude);
        return this;
    }


}
