package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CDistrict;
import com.devcamp.realestate.entity.CStreet;
import com.devcamp.realestate.entity.CWard;
import com.devcamp.realestate.entity.Cprovince;
import com.devcamp.realestate.repository.CDistrictRepository;
import com.devcamp.realestate.repository.CProvinceRepository;
import com.devcamp.realestate.repository.CStreetRepository;
import com.devcamp.realestate.repository.CWardRepository;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CProvinceController {
    @Autowired
    CProvinceRepository pProvinceRepository;

    @Autowired
    CDistrictRepository pDistrictRepository;

    @Autowired
    CWardRepository pCWardRepository;

    @Autowired 
    CStreetRepository pCStreetRepository;

    @GetMapping("/all-provinces")
    public ResponseEntity<List<Cprovince>> getAllProvince() {
        try {
            ArrayList<Cprovince> contructions = new ArrayList<>();
            pProvinceRepository.findAll().forEach(contructions::add);
            return new ResponseEntity<>(contructions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province/{id}")
    public ResponseEntity<Object> getProvinceById(@PathVariable("id") int id) {
        Optional<Cprovince> customerData = pProvinceRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/all-districts")
    public ResponseEntity<List<CDistrict>> getAllDistrict() {
        try {
            ArrayList<CDistrict> contructions = new ArrayList<>();
            pDistrictRepository.findAll().forEach(contructions::add);
            return new ResponseEntity<>(contructions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districtslike/{provinceId}")
    public ResponseEntity<List<CDistrict>> getDistrictLike(@PathVariable Integer provinceId) {
        try {
            ArrayList<CDistrict> contructions = new ArrayList<>();
            pDistrictRepository.findDistrictLike(provinceId).forEach(contructions::add);
            return new ResponseEntity<>(contructions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/district/{id}")
    public ResponseEntity<Object> getDistrictById(@PathVariable("id") int id) {
        Optional<CDistrict> customerData = pDistrictRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/all-wards")
    public ResponseEntity<List<CWard>> getAllWard() {
        try {
            ArrayList<CWard> contructions = new ArrayList<>();
            pCWardRepository.findAll().forEach(contructions::add);
            return new ResponseEntity<>(contructions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/wardslike/{districtId}")
    public ResponseEntity<List<CWard>> getWardLike(@PathVariable Integer districtId) {
        try {
            ArrayList<CWard> contructions = new ArrayList<>();
            pCWardRepository.findWardLike(districtId).forEach(contructions::add);
            return new ResponseEntity<>(contructions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/ward/{id}")
    public ResponseEntity<Object> getWardById(@PathVariable("id") int id) {
        Optional<CWard> customerData = pCWardRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/all-streets")
    public ResponseEntity<List<CStreet>> getAllStreet() {
        try {
            ArrayList<CStreet> contructions = new ArrayList<>();
            pCStreetRepository.findAll().forEach(contructions::add);
            return new ResponseEntity<>(contructions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/streetslike/{wardId}")
    public ResponseEntity<List<CStreet>> getStreetLike(@PathVariable Integer wardId) {
        try {
            ArrayList<CStreet> contructions = new ArrayList<>();
            pCStreetRepository.findStreetLike(wardId).forEach(contructions::add);
            return new ResponseEntity<>(contructions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/street/{id}")
    public ResponseEntity<Object> getStreetById(@PathVariable("id") int id) {
        Optional<CStreet> customerData = pCStreetRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
