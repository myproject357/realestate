package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CUtiliti;
import com.devcamp.realestate.repository.CUtilitiRepository;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CUtilitiController {
    @Autowired
    CUtilitiRepository pCUtilitiRepository;

    @GetMapping("/all-utiliti")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CUtiliti>> getAllMsLayout() {
        try {
            ArrayList<CUtiliti> customers = new ArrayList<>();
            pCUtilitiRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/utiliti/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CUtiliti> customerData = pCUtilitiRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/utiliti/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CUtiliti employee) {
        Optional<CUtiliti> customerData = pCUtilitiRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CUtiliti newEmployee = new CUtiliti();

           newEmployee.setDescription(employee.getDescription());
           newEmployee.setName(employee.getName());
           newEmployee.setPhoto(employee.getPhoto());

            CUtiliti saveRole = pCUtilitiRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/utiliti/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CUtiliti employee) {
        Optional<CUtiliti> customerData = pCUtilitiRepository.findById(id);
        if (customerData.isPresent()) {
            CUtiliti newEmployee = customerData.get();

            newEmployee.setDescription(employee.getDescription());
            newEmployee.setName(employee.getName());
            newEmployee.setPhoto(employee.getPhoto());

            CUtiliti saveRole = pCUtilitiRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/utiliti/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCUtilitiRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }      
}
