package com.devcamp.realestate.service;

import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.CRealEstate;

@Service
public class CRealEstateService {
    public void setGetRealEstateService(CRealEstate newEmployee , CRealEstate employee) {
        newEmployee.setAcreage(employee.getAcreage());
        newEmployee.setAddress(employee.getAddress());
        newEmployee.setAdjacent_alley_min_width(employee.getAdjacent_alley_min_width());
        newEmployee.setAdjacent_facade_num(employee.getAdjacent_facade_num());
        newEmployee.setAdjacent_road(employee.getAdjacent_road());
        newEmployee.setApart_code(employee.getApart_code());
        newEmployee.setApart_loca(employee.getApart_loca());
        newEmployee.setApart_type(employee.getApart_type());
        newEmployee.setBalcony(employee.getBalcony());
        newEmployee.setBath(employee.getBath());
        newEmployee.setBedroom(employee.getBedroom());
        newEmployee.setCLCL(employee.getCLCL());
        newEmployee.setCTXD_price(employee.getCTXD_price());
        newEmployee.setCTXD_value(employee.getCTXD_value());
        newEmployee.setCreate_by(employee.getCreate_by());
        newEmployee.setCustomer_id(employee.getCustomer_id());
        newEmployee.setDTSXD(employee.getDTSXD());
        newEmployee.setDate_create(employee.getDate_create());
        newEmployee.setDescription(employee.getDescription());
        newEmployee.setDirection(employee.getDirection());
        newEmployee.setDistance2facade(employee.getDistance2facade());
        newEmployee.setDistrict_id(employee.getDistrict_id());
        newEmployee.setFSBO(employee.getFSBO());
        newEmployee.setFactor(employee.getFactor());
        newEmployee.setFurniture_type(employee.getFurniture_type());
        newEmployee.setLandscape_view(employee.getLandscape_view());
        newEmployee.setLegal_doc(employee.getLegal_doc());
        newEmployee.setLong_x(employee.getLong_x());
        newEmployee.setNumber_floors(employee.getNumber_floors());
        newEmployee.setPhoto(employee.getPhoto());
        newEmployee.setPrice(employee.getPrice());
        newEmployee.setPrice_min(employee.getPrice_min());
        newEmployee.setPrice_rent(employee.getPrice_rent());
        newEmployee.setPrice_time(employee.getPrice_time());
        newEmployee.setProject_id(employee.getProject_id());
        newEmployee.setProvince_id(employee.getProvince_id());
        newEmployee.setRequest(employee.getRequest());
        newEmployee.setReturn_rate(employee.getReturn_rate());
        newEmployee.setShape(employee.getShape());
        newEmployee.setStreet_house(employee.getStreet_house());
        newEmployee.setStreet_id(employee.getStreet_id());
        newEmployee.setStructure(employee.getStructure());
        newEmployee.setTitle(employee.getTitle());
        newEmployee.setTotal_floors(employee.getTotal_floors());
        newEmployee.setType(employee.getType());
        newEmployee.setUpdate_by(employee.getUpdate_by());
        newEmployee.setView_num(employee.getView_num());
        newEmployee.setWall_area(employee.getWall_area());
        newEmployee.setWards_id(employee.getWards_id());
        newEmployee.setWidth_y(employee.getWidth_y());
        newEmployee.set_lat(employee.get_lat());
        newEmployee.set_lng(employee.get_lng());
        newEmployee.street_id(employee.getStreet_id());
        newEmployee.street_house(employee.getStreet_house());
    }
}
