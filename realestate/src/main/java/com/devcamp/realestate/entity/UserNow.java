package com.devcamp.realestate.entity;
import javax.persistence.*;


public class UserNow {
    // @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY)
    // private Integer id;

    private Long userId;

    private String userName;


    public UserNow() {
    }

    public UserNow(Long userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }


    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserNow userId(Long userId) {
        setUserId(userId);
        return this;
    }

    public UserNow userName(String userName) {
        setUserName(userName);
        return this;
    }

}
