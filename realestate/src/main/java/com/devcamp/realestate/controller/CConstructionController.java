package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CConstructionContrator;
import com.devcamp.realestate.repository.CConstructionContratorRepository;
import com.devcamp.realestate.service.CContructionService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CConstructionController {
    @Autowired
    CConstructionContratorRepository pCConstructionContratorRepository;

    @Autowired
    CContructionService pCContructionService;

    @GetMapping("/all-contructions")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CConstructionContrator>> getAllContruction() {
        try {
            ArrayList<CConstructionContrator> contructions = new ArrayList<>();
            pCConstructionContratorRepository.findAll().forEach(contructions::add);
            return new ResponseEntity<>(contructions, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/contruction/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CConstructionContrator> customerData = pCConstructionContratorRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/contruction/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CConstructionContrator employee) {
        Optional<CConstructionContrator> customerData = pCConstructionContratorRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CConstructionContrator newEmployee = new CConstructionContrator();

            pCContructionService.setGetContructionService(newEmployee, employee);

            CConstructionContrator saveRole = pCConstructionContratorRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/contruction/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CConstructionContrator employee) {
        Optional<CConstructionContrator> customerData = pCConstructionContratorRepository.findById(id);
        if (customerData.isPresent()) {
            CConstructionContrator newEmployee = customerData.get();

            pCContructionService.setGetContructionService(newEmployee, employee);

            CConstructionContrator saveRole = pCConstructionContratorRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/contruction/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCConstructionContratorRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }   
    

}
