package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CEmployee;

public interface CEmployeeRepository extends JpaRepository<CEmployee,Integer>{
    
}
