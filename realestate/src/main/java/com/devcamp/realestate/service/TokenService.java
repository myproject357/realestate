package com.devcamp.realestate.service;

import com.devcamp.realestate.entity.Token;

public interface TokenService {

    Token createToken(Token token);

    Token findByToken(String token);
}
