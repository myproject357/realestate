package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CDesignUnit;
import com.devcamp.realestate.repository.CDesignUnitRepository;
import com.devcamp.realestate.service.CDesignUnitService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CDesignUnitController {
    @Autowired 
    CDesignUnitRepository pCDesignUnitRepository;

    @Autowired
    CDesignUnitService pCDesignUnitService;

    @GetMapping("/all-designs")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CDesignUnit>> getAllDesign() {
        try {
            ArrayList<CDesignUnit> customers = new ArrayList<CDesignUnit>();
            pCDesignUnitRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/design/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CDesignUnit> customerData = pCDesignUnitRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/design/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CDesignUnit employee) {
        Optional<CDesignUnit> customerData = pCDesignUnitRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CDesignUnit newEmployee = new CDesignUnit();

            pCDesignUnitService.setGetDesignUnitService(newEmployee, employee);

            CDesignUnit saveRole = pCDesignUnitRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/design/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CDesignUnit employee) {
        Optional<CDesignUnit> customerData = pCDesignUnitRepository.findById(id);
        if (customerData.isPresent()) {
            CDesignUnit newEmployee = customerData.get();

            pCDesignUnitService.setGetDesignUnitService(newEmployee, employee);

            CDesignUnit saveRole = pCDesignUnitRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/design/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCDesignUnitRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
}
