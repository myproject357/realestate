package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CProject;
import com.devcamp.realestate.repository.CProjectRepository;
import com.devcamp.realestate.service.CProjectService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CProjectController {
    @Autowired
    CProjectRepository projectRepository;

    @Autowired
    CProjectService projectService;

    @GetMapping("/all-projects")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CProject>> getAllDesign() {
        try {
            ArrayList<CProject> customers = new ArrayList<>();
            projectRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all-projectsc")
    public ResponseEntity<List<CProject>> getAllDesignC() {
        try {
            ArrayList<CProject> customers = new ArrayList<>();
            projectRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/project/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") Integer id) {
        Optional<CProject> customerData = projectRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/projectsale/{sale}")
    public ResponseEntity<Object> getCustomerBySale(@PathVariable("sale") Integer sale) {
        try {
            ArrayList<CProject> customers = new ArrayList<>();
            projectRepository.findProjectBySale(sale).forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/project/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CProject employee) {
        Optional<CProject> customerData = projectRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CProject newEmployee = new CProject();
            

            projectService.setGetProjectService(newEmployee, employee);

           CProject saveRole = projectRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/project/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CProject employee) {
        Optional<CProject> customerData = projectRepository.findById(id);
        if (customerData.isPresent()) {
            CProject newEmployee = customerData.get();

            projectService.setGetProjectService(newEmployee, employee);

            CProject saveRole = projectRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/project/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            projectRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }  
}
