package com.devcamp.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.entity.CProject;

public interface CProjectRepository extends JpaRepository<CProject,Integer>{
    @Query(value = "SELECT * FROM project WHERE sale LIKE :sale%", nativeQuery = true)
	List<CProject> findProjectBySale(@Param("sale") Integer sale);
}
