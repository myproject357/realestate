package com.devcamp.realestate.entity;

import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class CProject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String _name;

    private Integer _province_id;

    private Integer _district_id;

    private Integer _ward_id;

    private Integer _street_id;

    private String address;

    private String slogan;

    private String description;

    private Float acreage;

    private Float construct_area;

    private Integer num_block;

    private String num_floors;

    private Integer num_apartment;

    private String apartmentt_area;

    private Integer investor;

    private Integer construction_contractor;

    @Column(name = "design_unit")
    private Integer designUnit;

    private String utilities;

    private String region_link;

    private String photo;

    private Double _lat;

    private Double _lng;

    private Integer sale;

    private String linkGoogle;

    private Integer reason;

    public CProject() {
    }

    public CProject(Integer id, String _name, Integer _province_id, Integer _district_id, Integer _ward_id, Integer _street_id,
            String address, String slogan, String description, Float acreage, Float construct_area, Integer num_block,
            String num_floors, Integer num_apartment, String apartmentt_area, Integer investor, Integer construction_contractor,
            Integer designUnit, String utilities, String region_link, String photo, Double _lat, Double _lng , Integer sale , String linkGoogle , Integer reason) {
        this.id = id;
        this._name = _name;
        this._province_id = _province_id;
        this._district_id = _district_id;
        this._ward_id = _ward_id;
        this._street_id = _street_id;
        this.address = address;
        this.slogan = slogan;
        this.description = description;
        this.acreage = acreage;
        this.construct_area = construct_area;
        this.num_block = num_block;
        this.num_floors = num_floors;
        this.num_apartment = num_apartment;
        this.apartmentt_area = apartmentt_area;
        this.investor = investor;
        this.construction_contractor = construction_contractor;
        this.designUnit = designUnit;
        this.utilities = utilities;
        this.region_link = region_link;
        this.photo = photo;
        this._lat = _lat;
        this._lng = _lng;
        this.sale = sale;
        this.linkGoogle = linkGoogle;
        this.reason = reason;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public Integer get_province_id() {
        return _province_id;
    }

    public void set_province_id(Integer _province_id) {
        this._province_id = _province_id;
    }

    public Integer get_district_id() {
        return _district_id;
    }

    public void set_district_id(Integer _district_id) {
        this._district_id = _district_id;
    }

    public Integer get_ward_id() {
        return _ward_id;
    }

    public void set_ward_id(Integer _ward_id) {
        this._ward_id = _ward_id;
    }

    public Integer get_street_id() {
        return _street_id;
    }

    public void set_street_id(Integer _street_id) {
        this._street_id = _street_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getAcreage() {
        return acreage;
    }

    public void setAcreage(Float acreage) {
        this.acreage = acreage;
    }

    public Float getConstruct_area() {
        return construct_area;
    }

    public void setConstruct_area(Float construct_area) {
        this.construct_area = construct_area;
    }

    public Integer getNum_block() {
        return num_block;
    }

    public void setNum_block(Integer num_block) {
        this.num_block = num_block;
    }

    public String getNum_floors() {
        return num_floors;
    }

    public void setNum_floors(String num_floors) {
        this.num_floors = num_floors;
    }

    public Integer getNum_apartment() {
        return num_apartment;
    }

    public void setNum_apartment(Integer num_apartment) {
        this.num_apartment = num_apartment;
    }

    public String getApartmentt_area() {
        return apartmentt_area;
    }

    public void setApartmentt_area(String apartmentt_area) {
        this.apartmentt_area = apartmentt_area;
    }

    public Integer getInvestor() {
        return investor;
    }

    public void setInvestor(Integer investor) {
        this.investor = investor;
    }

    public Integer getConstruction_contractor() {
        return construction_contractor;
    }

    public void setConstruction_contractor(Integer construction_contractor) {
        this.construction_contractor = construction_contractor;
    }

    public Integer getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(Integer designUnit) {
        this.designUnit = designUnit;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegion_link() {
        return region_link;
    }

    public void setRegion_link(String region_link) {
        this.region_link = region_link;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double get_lat() {
        return _lat;
    }

    public void set_lat(Double _lat) {
        this._lat = _lat;
    }

    public Double get_lng() {
        return _lng;
    }

    public void set_lng(Double _lng) {
        this._lng = _lng;
    }

    public Integer getSale() {
        return sale;
    }

    public void setSale(Integer sale) {
        this.sale = sale;
    }

    public String getLinkGoogle() {
        return linkGoogle;
    }

    public void setLinkGoogle(String linkGoogle) {
        this.linkGoogle = linkGoogle;
    }

    public Integer getReason() {
        return reason;
    }

    public void setReason(Integer reason) {
        this.reason = reason;
    }

}
