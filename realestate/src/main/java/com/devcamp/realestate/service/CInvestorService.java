package com.devcamp.realestate.service;

import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.CInvestor;

@Service
public class CInvestorService {
    public void setGetInvestorService(CInvestor newInvestor, CInvestor investor) {
        newInvestor.setName(investor.getName());
        newInvestor.setAddress(investor.getAddress());
        newInvestor.setDescription(investor.getDescription());
        newInvestor.setEmail(investor.getEmail());
        newInvestor.setFax(investor.getFax());
        newInvestor.setNote(investor.getNote());
        newInvestor.setPhone(investor.getPhone());
        newInvestor.setPhone2(investor.getPhone2());
        newInvestor.setProjects(investor.getProjects());
        newInvestor.setWebsite(investor.getWebsite());
    }
}
