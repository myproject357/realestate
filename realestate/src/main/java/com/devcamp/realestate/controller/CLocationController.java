package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CLocation;
import com.devcamp.realestate.repository.CLocationRepository;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CLocationController {
    @Autowired
    CLocationRepository pCLocationRepository;

    @GetMapping("/all-location")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CLocation>> getAllEmp() {
        try {
            ArrayList<CLocation> customers = new ArrayList<>();
            pCLocationRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/location/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CLocation> customerData = pCLocationRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/location/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CLocation employee) {
        Optional<CLocation> customerData = pCLocationRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CLocation newEmployee = new CLocation();

           newEmployee.setLatitude(employee.getLatitude());
           newEmployee.setLatitude(employee.getLongitude());

           CLocation saveRole = pCLocationRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/location/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CLocation employee) {
        Optional<CLocation> customerData = pCLocationRepository.findById(id);
        if (customerData.isPresent()) {
            CLocation newEmployee = customerData.get();

            newEmployee.setLatitude(employee.getLatitude());
            newEmployee.setLatitude(employee.getLongitude());

            CLocation saveRole = pCLocationRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/location/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCLocationRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }  
}
