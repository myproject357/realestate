package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CMasterLayout;

public interface CMasterLayoutRepository extends JpaRepository<CMasterLayout,Integer>{
    
}
