package com.devcamp.realestate.service;

import com.devcamp.realestate.entity.User;
import com.devcamp.realestate.security.UserPrincipal;

public interface UserService {
    User createUser(User user);

    UserPrincipal findByUsername(String username);
}
