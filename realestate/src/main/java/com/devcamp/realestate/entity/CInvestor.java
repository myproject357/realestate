package com.devcamp.realestate.entity;
import javax.persistence.*;
import javax.persistence.Table;
@Entity
@Table(name = "investor")
public class CInvestor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String description;

    private String projects;

    private Integer address;

    private String phone;

    private String phone2;

    private String fax;

    private String email;

    private String website;

    private String note;


    public CInvestor() {
    }

    public CInvestor(int id, String name, String description, String projects, Integer address, String phone, String phone2, String fax, String email, String website, String note) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projects = projects;
        this.address = address;
        this.phone = phone;
        this.phone2 = phone2;
        this.fax = fax;
        this.email = email;
        this.website = website;
        this.note = note;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjects() {
        return this.projects;
    }

    public void setProjects(String projects) {
        this.projects = projects;
    }

    public Integer getAddress() {
        return this.address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return this.phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return this.website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public CInvestor id(int id) {
        setId(id);
        return this;
    }

    public CInvestor name(String name) {
        setName(name);
        return this;
    }

    public CInvestor description(String description) {
        setDescription(description);
        return this;
    }

    public CInvestor projects(String projects) {
        setProjects(projects);
        return this;
    }

    public CInvestor address(Integer address) {
        setAddress(address);
        return this;
    }

    public CInvestor phone(String phone) {
        setPhone(phone);
        return this;
    }

    public CInvestor phone2(String phone2) {
        setPhone2(phone2);
        return this;
    }

    public CInvestor fax(String fax) {
        setFax(fax);
        return this;
    }

    public CInvestor email(String email) {
        setEmail(email);
        return this;
    }

    public CInvestor website(String website) {
        setWebsite(website);
        return this;
    }

    public CInvestor note(String note) {
        setNote(note);
        return this;
    }

}
