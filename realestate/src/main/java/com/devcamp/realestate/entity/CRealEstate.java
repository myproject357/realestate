package com.devcamp.realestate.entity;
import java.util.Date;

import javax.persistence.*;
import javax.persistence.Table;
@Entity
@Table(name = "realestate")
public class CRealEstate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private Long type;

    private Long request;

    private Long province_id;

    private Long district_id;

    private Long wards_id;

    private Long street_id;

    private Long project_id;

    private String address;

    private Long customer_id;

    private Long price;

    private Long price_min;

    private Long price_time;

    @Temporal(TemporalType.TIMESTAMP)
    private Date date_create;

    private Double acreage;

    private Long direction;

    private Long total_floors;

    private Long number_floors;

    private Long bath;

    private String apart_code;

    private Double wall_area;

    private Long bedroom;

    private Long balcony;

    private String landscape_view;

    private Long apart_loca;

    private Long apart_type;

    private Long furniture_type;

    private Long price_rent;

    private Double return_rate;

    private Long legal_doc;

    private String description;

    private Long width_y;

    private Long long_x;

    private Long street_house;

    private Long FSBO;

    private Long view_num;

    private Long create_by;

    private Long update_by;

    private String shape;

    private Long distance2facade;

    private Long adjacent_facade_num;

    private String adjacent_road;

    private Long alley_min_width;

    private Long adjacent_alley_min_width;

    private Long factor;

    private String structure;

    private Long DTSXD;

    private Long CLCL;

    private Long CTXD_price;

    private Long CTXD_value;

    private String photo;

    private Double _lat;

    private Double _lng;

    private Integer Comfirm;

    private String Photo1;

    private String Photo2;

    private String Photo3;

    private Integer recommend;

    public CRealEstate() {
    }

    public CRealEstate(Long id, String title, Long type, Long request, Long province_id, Long district_id, Long wards_id, Long street_id, 
    Long project_id, String address, Long customer_id, Long price, Long price_min, Long price_time, Date date_create, Double acreage, 
    Long direction, Long total_floors, Long number_floors, Long bath, String apart_code, Double wall_area, Long bedroom, Long balcony, 
    String landscape_view, Long apart_loca, Long apart_type, Long furniture_type, Long price_rent, Double return_rate, Long legal_doc, String description, 
    Long width_y, Long long_x, Long street_house, Long FSBO, Long view_num, Long create_by, Long update_by, String shape, Long distance2facade, 
    Long adjacent_facade_num, String adjacent_road, Long alley_min_width, Long adjacent_alley_min_width, Long factor, String structure, Long DTSXD, 
    Long CLCL, Long CTXD_price, Long CTXD_value, String photo, Double _lat, Double _lng, Integer comfirm, String photo1 , String photo2 , String photo3 , Integer recommend) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.request = request;
        this.province_id = province_id;
        this.district_id = district_id;
        this.wards_id = wards_id;
        this.street_id = street_id;
        this.project_id = project_id;
        this.address = address;
        this.customer_id = customer_id;
        this.price = price;
        this.price_min = price_min;
        this.price_time = price_time;
        this.date_create = date_create;
        this.acreage = acreage;
        this.direction = direction;
        this.total_floors = total_floors;
        this.number_floors = number_floors;
        this.bath = bath;
        this.apart_code = apart_code;
        this.wall_area = wall_area;
        this.bedroom = bedroom;
        this.balcony = balcony;
        this.landscape_view = landscape_view;
        this.apart_loca = apart_loca;
        this.apart_type = apart_type;
        this.furniture_type = furniture_type;
        this.price_rent = price_rent;
        this.return_rate = return_rate;
        this.legal_doc = legal_doc;
        this.description = description;
        this.width_y = width_y;
        this.long_x = long_x;
        this.street_house = street_house;
        this.FSBO = FSBO;
        this.view_num = view_num;
        this.create_by = create_by;
        this.update_by = update_by;
        this.shape = shape;
        this.distance2facade = distance2facade;
        this.adjacent_facade_num = adjacent_facade_num;
        this.adjacent_road = adjacent_road;
        this.alley_min_width = alley_min_width;
        this.adjacent_alley_min_width = adjacent_alley_min_width;
        this.factor = factor;
        this.structure = structure;
        this.DTSXD = DTSXD;
        this.CLCL = CLCL;
        this.CTXD_price = CTXD_price;
        this.CTXD_value = CTXD_value;
        this.photo = photo;
        this._lat = _lat;
        this._lng = _lng;
        this.Comfirm = comfirm;
        this.Photo1 = photo1;
        this.Photo2 = photo2;
        this.Photo3 = photo3;
        this.recommend = recommend;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getType() {
        return this.type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getRequest() {
        return this.request;
    }

    public void setRequest(Long request) {
        this.request = request;
    }

    public Long getProvince_id() {
        return this.province_id;
    }

    public void setProvince_id(Long province_id) {
        this.province_id = province_id;
    }

    public Long getDistrict_id() {
        return this.district_id;
    }

    public void setDistrict_id(Long district_id) {
        this.district_id = district_id;
    }

    public Long getWards_id() {
        return this.wards_id;
    }

    public void setWards_id(Long wards_id) {
        this.wards_id = wards_id;
    }

    public Long getStreet_id() {
        return this.street_id;
    }

    public void setStreet_id(Long street_id) {
        this.street_id = street_id;
    }

    public Long getProject_id() {
        return this.project_id;
    }

    public void setProject_id(Long project_id) {
        this.project_id = project_id;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCustomer_id() {
        return this.customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        this.customer_id = customer_id;
    }

    public Long getPrice() {
        return this.price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getPrice_min() {
        return this.price_min;
    }

    public void setPrice_min(Long price_min) {
        this.price_min = price_min;
    }

    public Long getPrice_time() {
        return this.price_time;
    }

    public void setPrice_time(Long price_time) {
        this.price_time = price_time;
    }

    public Date getDate_create() {
        return this.date_create;
    }

    public void setDate_create(Date date_create) {
        this.date_create = date_create;
    }

    public Double getAcreage() {
        return this.acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public Long getDirection() {
        return this.direction;
    }

    public void setDirection(Long direction) {
        this.direction = direction;
    }

    public Long getTotal_floors() {
        return this.total_floors;
    }

    public void setTotal_floors(Long total_floors) {
        this.total_floors = total_floors;
    }

    public Long getNumber_floors() {
        return this.number_floors;
    }

    public void setNumber_floors(Long number_floors) {
        this.number_floors = number_floors;
    }

    public Long getBath() {
        return this.bath;
    }

    public void setBath(Long bath) {
        this.bath = bath;
    }

    public String getApart_code() {
        return this.apart_code;
    }

    public void setApart_code(String apart_code) {
        this.apart_code = apart_code;
    }

    public Double getWall_area() {
        return this.wall_area;
    }

    public void setWall_area(Double wall_area) {
        this.wall_area = wall_area;
    }

    public Long getBedroom() {
        return this.bedroom;
    }

    public void setBedroom(Long bedroom) {
        this.bedroom = bedroom;
    }

    public Long getBalcony() {
        return this.balcony;
    }

    public void setBalcony(Long balcony) {
        this.balcony = balcony;
    }

    public String getLandscape_view() {
        return this.landscape_view;
    }

    public void setLandscape_view(String landscape_view) {
        this.landscape_view = landscape_view;
    }

    public Long getApart_loca() {
        return this.apart_loca;
    }

    public void setApart_loca(Long apart_loca) {
        this.apart_loca = apart_loca;
    }

    public Long getApart_type() {
        return this.apart_type;
    }

    public void setApart_type(Long apart_type) {
        this.apart_type = apart_type;
    }

    public Long getFurniture_type() {
        return this.furniture_type;
    }

    public void setFurniture_type(Long furniture_type) {
        this.furniture_type = furniture_type;
    }

    public Long getPrice_rent() {
        return this.price_rent;
    }

    public void setPrice_rent(Long price_rent) {
        this.price_rent = price_rent;
    }

    public Double getReturn_rate() {
        return this.return_rate;
    }

    public void setReturn_rate(Double return_rate) {
        this.return_rate = return_rate;
    }

    public Long getLegal_doc() {
        return this.legal_doc;
    }

    public void setLegal_doc(Long legal_doc) {
        this.legal_doc = legal_doc;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getWidth_y() {
        return this.width_y;
    }

    public void setWidth_y(Long width_y) {
        this.width_y = width_y;
    }

    public Long getLong_x() {
        return this.long_x;
    }

    public void setLong_x(Long long_x) {
        this.long_x = long_x;
    }

    public Long getStreet_house() {
        return this.street_house;
    }

    public void setStreet_house(Long street_house) {
        this.street_house = street_house;
    }

    public Long getFSBO() {
        return this.FSBO;
    }

    public void setFSBO(Long FSBO) {
        this.FSBO = FSBO;
    }

    public Long getView_num() {
        return this.view_num;
    }

    public void setView_num(Long view_num) {
        this.view_num = view_num;
    }

    public Long getCreate_by() {
        return this.create_by;
    }

    public void setCreate_by(Long create_by) {
        this.create_by = create_by;
    }

    public Long getUpdate_by() {
        return this.update_by;
    }

    public void setUpdate_by(Long update_by) {
        this.update_by = update_by;
    }

    public String getShape() {
        return this.shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Long getDistance2facade() {
        return this.distance2facade;
    }

    public void setDistance2facade(Long distance2facade) {
        this.distance2facade = distance2facade;
    }

    public Long getAdjacent_facade_num() {
        return this.adjacent_facade_num;
    }

    public void setAdjacent_facade_num(Long adjacent_facade_num) {
        this.adjacent_facade_num = adjacent_facade_num;
    }

    public String getAdjacent_road() {
        return this.adjacent_road;
    }

    public void setAdjacent_road(String adjacent_road) {
        this.adjacent_road = adjacent_road;
    }

    public Long getAlley_min_width() {
        return this.alley_min_width;
    }

    public void setAlley_min_width(Long alley_min_width) {
        this.alley_min_width = alley_min_width;
    }

    public Long getAdjacent_alley_min_width() {
        return this.adjacent_alley_min_width;
    }

    public void setAdjacent_alley_min_width(Long adjacent_alley_min_width) {
        this.adjacent_alley_min_width = adjacent_alley_min_width;
    }

    public Long getFactor() {
        return this.factor;
    }

    public void setFactor(Long factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return this.structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Long getDTSXD() {
        return this.DTSXD;
    }

    public void setDTSXD(Long DTSXD) {
        this.DTSXD = DTSXD;
    }

    public Long getCLCL() {
        return this.CLCL;
    }

    public void setCLCL(Long CLCL) {
        this.CLCL = CLCL;
    }

    public Long getCTXD_price() {
        return this.CTXD_price;
    }

    public void setCTXD_price(Long CTXD_price) {
        this.CTXD_price = CTXD_price;
    }

    public Long getCTXD_value() {
        return this.CTXD_value;
    }

    public void setCTXD_value(Long CTXD_value) {
        this.CTXD_value = CTXD_value;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double get_lat() {
        return this._lat;
    }

    public void set_lat(Double _lat) {
        this._lat = _lat;
    }

    public Double get_lng() {
        return this._lng;
    }

    public void set_lng(Double _lng) {
        this._lng = _lng;
    }

    public Integer getComfirm() {
        return this.Comfirm;
    }

    public void setComfirm(Integer comfirm) {
        this.Comfirm = comfirm;
    }

    public CRealEstate id(Long id) {
        setId(id);
        return this;
    }

    public CRealEstate title(String title) {
        setTitle(title);
        return this;
    }

    public CRealEstate type(Long type) {
        setType(type);
        return this;
    }

    public CRealEstate request(Long request) {
        setRequest(request);
        return this;
    }

    public CRealEstate province_id(Long province_id) {
        setProvince_id(province_id);
        return this;
    }

    public CRealEstate district_id(Long district_id) {
        setDistrict_id(district_id);
        return this;
    }

    public CRealEstate wards_id(Long wards_id) {
        setWards_id(wards_id);
        return this;
    }

    public CRealEstate street_id(Long street_id) {
        setStreet_id(street_id);
        return this;
    }

    public CRealEstate project_id(Long project_id) {
        setProject_id(project_id);
        return this;
    }

    public CRealEstate address(String address) {
        setAddress(address);
        return this;
    }

    public CRealEstate customer_id(Long customer_id) {
        setCustomer_id(customer_id);
        return this;
    }

    public CRealEstate price(Long price) {
        setPrice(price);
        return this;
    }

    public CRealEstate price_min(Long price_min) {
        setPrice_min(price_min);
        return this;
    }

    public CRealEstate price_time(Long price_time) {
        setPrice_time(price_time);
        return this;
    }

    public CRealEstate date_create(Date date_create) {
        setDate_create(date_create);
        return this;
    }

    public CRealEstate acreage(Double acreage) {
        setAcreage(acreage);
        return this;
    }

    public CRealEstate direction(Long direction) {
        setDirection(direction);
        return this;
    }

    public CRealEstate total_floors(Long total_floors) {
        setTotal_floors(total_floors);
        return this;
    }

    public CRealEstate number_floors(Long number_floors) {
        setNumber_floors(number_floors);
        return this;
    }

    public CRealEstate bath(Long bath) {
        setBath(bath);
        return this;
    }

    public CRealEstate apart_code(String apart_code) {
        setApart_code(apart_code);
        return this;
    }

    public CRealEstate wall_area(Double wall_area) {
        setWall_area(wall_area);
        return this;
    }

    public CRealEstate bedroom(Long bedroom) {
        setBedroom(bedroom);
        return this;
    }

    public CRealEstate balcony(Long balcony) {
        setBalcony(balcony);
        return this;
    }

    public CRealEstate landscape_view(String landscape_view) {
        setLandscape_view(landscape_view);
        return this;
    }

    public CRealEstate apart_loca(Long apart_loca) {
        setApart_loca(apart_loca);
        return this;
    }

    public CRealEstate apart_type(Long apart_type) {
        setApart_type(apart_type);
        return this;
    }

    public CRealEstate furniture_type(Long furniture_type) {
        setFurniture_type(furniture_type);
        return this;
    }

    public CRealEstate price_rent(Long price_rent) {
        setPrice_rent(price_rent);
        return this;
    }

    public CRealEstate return_rate(Double return_rate) {
        setReturn_rate(return_rate);
        return this;
    }

    public CRealEstate legal_doc(Long legal_doc) {
        setLegal_doc(legal_doc);
        return this;
    }

    public CRealEstate description(String description) {
        setDescription(description);
        return this;
    }

    public CRealEstate width_y(Long width_y) {
        setWidth_y(width_y);
        return this;
    }

    public CRealEstate long_x(Long long_x) {
        setLong_x(long_x);
        return this;
    }

    public CRealEstate street_house(Long street_house) {
        setStreet_house(street_house);
        return this;
    }

    public CRealEstate FSBO(Long FSBO) {
        setFSBO(FSBO);
        return this;
    }

    public CRealEstate view_num(Long view_num) {
        setView_num(view_num);
        return this;
    }

    public CRealEstate create_by(Long create_by) {
        setCreate_by(create_by);
        return this;
    }

    public CRealEstate update_by(Long update_by) {
        setUpdate_by(update_by);
        return this;
    }

    public CRealEstate shape(String shape) {
        setShape(shape);
        return this;
    }

    public CRealEstate distance2facade(Long distance2facade) {
        setDistance2facade(distance2facade);
        return this;
    }

    public CRealEstate adjacent_facade_num(Long adjacent_facade_num) {
        setAdjacent_facade_num(adjacent_facade_num);
        return this;
    }

    public CRealEstate adjacent_road(String adjacent_road) {
        setAdjacent_road(adjacent_road);
        return this;
    }

    public CRealEstate alley_min_width(Long alley_min_width) {
        setAlley_min_width(alley_min_width);
        return this;
    }

    public CRealEstate adjacent_alley_min_width(Long adjacent_alley_min_width) {
        setAdjacent_alley_min_width(adjacent_alley_min_width);
        return this;
    }

    public CRealEstate factor(Long factor) {
        setFactor(factor);
        return this;
    }

    public CRealEstate structure(String structure) {
        setStructure(structure);
        return this;
    }

    public CRealEstate DTSXD(Long DTSXD) {
        setDTSXD(DTSXD);
        return this;
    }

    public CRealEstate CLCL(Long CLCL) {
        setCLCL(CLCL);
        return this;
    }

    public CRealEstate CTXD_price(Long CTXD_price) {
        setCTXD_price(CTXD_price);
        return this;
    }

    public CRealEstate CTXD_value(Long CTXD_value) {
        setCTXD_value(CTXD_value);
        return this;
    }

    public CRealEstate photo(String photo) {
        setPhoto(photo);
        return this;
    }

    public CRealEstate _lat(Double _lat) {
        set_lat(_lat);
        return this;
    }

    public CRealEstate _lng(Double _lng) {
        set_lng(_lng);
        return this;
    }

    public String getPhoto1() {
        return Photo1;
    }

    public void setPhoto1(String photo1) {
        Photo1 = photo1;
    }

    public String getPhoto2() {
        return Photo2;
    }

    public void setPhoto2(String photo2) {
        Photo2 = photo2;
    }

    public String getPhoto3() {
        return Photo3;
    }

    public void setPhoto3(String photo3) {
        Photo3 = photo3;
    }

    public Integer getRecommend() {
        return recommend;
    }

    public void setRecommend(Integer recommend) {
        this.recommend = recommend;
    }


}
