package com.devcamp.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CAddressMap;
import com.devcamp.realestate.repository.CAddressMapRepository;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CAddressMapController {
    @Autowired
    CAddressMapRepository pAddressMapRepository;

    @GetMapping("/all-addressmap")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CAddressMap>> getAllEmp() {
        try {
            ArrayList<CAddressMap> customers = new ArrayList<>();
            pAddressMapRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/addressmap/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CAddressMap> customerData = pAddressMapRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addressmap/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CAddressMap employee) {
        Optional<CAddressMap> customerData = pAddressMapRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CAddressMap newEmployee = new CAddressMap();

            newEmployee.setAddress(employee.getAddress());
            newEmployee.set_lat(employee.get_lat());
            newEmployee.set_lng(employee.get_lng());

            CAddressMap saveRole = pAddressMapRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/addressmap/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id,
            @Validated @RequestBody CAddressMap employee) {
        Optional<CAddressMap> customerData = pAddressMapRepository.findById(id);
        if (customerData.isPresent()) {
            CAddressMap newEmployee = customerData.get();

            newEmployee.setAddress(employee.getAddress());
            newEmployee.set_lat(employee.get_lat());
            newEmployee.set_lng(employee.get_lng());

            CAddressMap saveRole = pAddressMapRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/addressmap/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id) {
        try {
            pAddressMapRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
