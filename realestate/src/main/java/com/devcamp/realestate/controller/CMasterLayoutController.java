package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CMasterLayout;
import com.devcamp.realestate.repository.CMasterLayoutRepository;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CMasterLayoutController {
    @Autowired 
    CMasterLayoutRepository pCMasterLayoutRepository;

    @GetMapping("/all-masterlayout")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CMasterLayout>> getAllMsLayout() {
        try {
            ArrayList<CMasterLayout> customers = new ArrayList<>();
            pCMasterLayoutRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/masterlayout/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CMasterLayout> customerData = pCMasterLayoutRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/masterlayout/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CMasterLayout employee) {
        Optional<CMasterLayout> customerData = pCMasterLayoutRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CMasterLayout newEmployee = new CMasterLayout();

           newEmployee.setName(employee.getName());
           newEmployee.setPhoto(employee.getPhoto());
           newEmployee.setAcreage(employee.getAcreage());
           newEmployee.setApartment_list(employee.getApartment_list());
           newEmployee.setCreateDate(employee.getCreateDate());
           newEmployee.setDescription(employee.getDescription());
           newEmployee.setProject_id(employee.getProject_id());
           newEmployee.setUpdateDate(employee.getUpdateDate());

           CMasterLayout saveRole = pCMasterLayoutRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/masterlayout/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CMasterLayout employee) {
        Optional<CMasterLayout> customerData = pCMasterLayoutRepository.findById(id);
        if (customerData.isPresent()) {
            CMasterLayout newEmployee = customerData.get();

            newEmployee.setName(employee.getName());
            newEmployee.setPhoto(employee.getPhoto());
            newEmployee.setAcreage(employee.getAcreage());
            newEmployee.setApartment_list(employee.getApartment_list());
            newEmployee.setCreateDate(employee.getCreateDate());
            newEmployee.setDescription(employee.getDescription());
            newEmployee.setProject_id(employee.getProject_id());
            newEmployee.setUpdateDate(employee.getUpdateDate());

            CMasterLayout saveRole = pCMasterLayoutRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/masterlayout/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCMasterLayoutRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }  
}
