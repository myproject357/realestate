$(document).ready(function() {
    //----Phần này làm sau khi đã làm trang info.js
    //Kiểm tra token nếu có token tức người dùng đã đăng nhập
    const token = getCookie("token");
    var gData = {
        username:"",
        password:""
    }
    if(token) {
        window.location.href = "index.html";
    }
    // ----Phần này làm sau khi đã làm trang info.js

    //Sự kiện bấm nút login
    $("#submit").on("click", function() {      
        saveData(gData);
        console.log(gData);
        if(validateForm(gData)) {
        signinForm();
        }
    });

    function signinForm() {
        var loginUrl = "http://localhost:8080/login";

        $.ajax({
            type: "POST",
            url: loginUrl,
            data: JSON.stringify(gData), 
            contentType: "application/json;charset=UTF-8",
            // cache : false,
            processData: false,
            success: function(responseObject) {
                responseHandler(responseObject);
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });
    }

    //Xử lý object trả về khi login thành công
    function responseHandler(data) {
        //Lưu token vào cookie trong 1 ngày
        setCookie("token", data, 1);

        window.location.href = "index.html";
    }

    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }


    //Validate dữ liệu từ form
    function validateForm(paramRes) {
        if(paramRes.username === "") {
            alert("User không phù hợp");
            return false;
        }

        if(paramRes.password === "") {
            alert("Password không phù hợp");
            return false;
        }

        return true;
    }

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function saveData(paramRes) {
        paramRes.username = $("#inputUser").val().trim();
        paramRes.password = $("#inputPassword").val().trim();
    }

            //Set sự kiện cho nút logout
            $("#out").on("click", function() {
                redirectToLogin();
            });
        
            function redirectToLogin() {
                // Trước khi logout cần xóa token đã lưu trong cookie
                setCookie("token", "", 1);
                window.location.href = "login.html";
            }
});