package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CUtiliti;

public interface CUtilitiRepository extends JpaRepository<CUtiliti,Integer>{
    
}
