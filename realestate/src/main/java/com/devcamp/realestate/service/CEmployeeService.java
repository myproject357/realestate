package com.devcamp.realestate.service;

import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.CEmployee;

@Service
public class CEmployeeService {
    public void setGetEmployeeService(CEmployee newEmployee , CEmployee employee) {
        newEmployee.setAddress(employee.getAddress());
        newEmployee.setBirthDate(employee.getBirthDate());
        newEmployee.setCity(employee.getCity());
        newEmployee.setCountry(employee.getCountry());
        newEmployee.setEmail(employee.getEmail());
        newEmployee.setExtension(employee.getExtension());
        newEmployee.setHireDate(employee.getHireDate());
        newEmployee.setHomePhone(employee.getHomePhone());
        newEmployee.setFirstName(employee.getFirstName());
        newEmployee.setLastName(employee.getLastName());
        newEmployee.setNotes(employee.getNotes());
        newEmployee.setUsername(employee.getUsername());
        newEmployee.setPassword(employee.getPassword());
        newEmployee.setUserLevel(employee.getUserLevel());
        newEmployee.setNotes(employee.getNotes());
        newEmployee.setPhoto(employee.getPhoto());
        newEmployee.setPostalCode(employee.getPostalCode());
        newEmployee.setProfile(employee.getProfile());
        newEmployee.setRegion(employee.getRegion());
        newEmployee.setReportsTo(employee.getReportsTo());
        newEmployee.setTitle(employee.getTitle());
        newEmployee.setTitleOfCourtesy(employee.getTitleOfCourtesy());
    }
}
