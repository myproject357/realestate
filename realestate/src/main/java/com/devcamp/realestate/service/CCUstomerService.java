package com.devcamp.realestate.service;

import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.CCustomer;

@Service
public class CCUstomerService {
    public void setGetCustomerService(CCustomer newCustomer , CCustomer customer) {
        newCustomer.setContact_name(customer.getContact_name());
        newCustomer.setContact_title(customer.getContact_title());
        newCustomer.setAddress(customer.getAddress());
        newCustomer.setCreate_by(customer.getCreate_by());
        newCustomer.setCreate_date(customer.getCreate_date());
        newCustomer.setEmail(customer.getEmail());
        newCustomer.setMobile(customer.getMobile());
        newCustomer.setNote(customer.getNote());
        newCustomer.setUpdate_by(customer.getUpdate_by());
        newCustomer.setUpdate_date(customer.getUpdate_date());
    }
}
