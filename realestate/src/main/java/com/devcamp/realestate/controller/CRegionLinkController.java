package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CRegionLink;
import com.devcamp.realestate.repository.CRegionLinkRepository;
import com.devcamp.realestate.service.CRegionLinkService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CRegionLinkController {
    @Autowired
    CRegionLinkRepository pCRegionLinkRepository;

    @Autowired
    CRegionLinkService pCRegionLinkService;

    @GetMapping("/all-regionlink")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CRegionLink>> getAllMsLayout() {
        try {
            ArrayList<CRegionLink> customers = new ArrayList<>();
            pCRegionLinkRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/regionlink/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CRegionLink> customerData = pCRegionLinkRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/regionlink/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CRegionLink employee) {
        Optional<CRegionLink> customerData = pCRegionLinkRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CRegionLink newEmployee = new CRegionLink();

            pCRegionLinkService.setGetRegionLinkService(newEmployee, employee);

           CRegionLink saveRole = pCRegionLinkRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/regionlink/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CRegionLink employee) {
        Optional<CRegionLink> customerData = pCRegionLinkRepository.findById(id);
        if (customerData.isPresent()) {
            CRegionLink newEmployee = customerData.get();

            pCRegionLinkService.setGetRegionLinkService(newEmployee, employee);

            CRegionLink saveRole = pCRegionLinkRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/regionlink/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCRegionLinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }  
}
