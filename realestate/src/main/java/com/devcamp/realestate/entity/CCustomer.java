package com.devcamp.realestate.entity;

import java.util.Date;

import javax.persistence.*;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "customers")
public class CCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String contact_name;

    private String contact_title;

    private String address;
    private String mobile;
    private String email;
    private String note;

    // @Column(name = "create_by")
    private Integer create_by;

    // @Column(name = "update_by")
    private Integer update_by;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date create_date;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date update_date;


    public CCustomer() {
    }

    public CCustomer(Integer id, String contact_name, String contact_title, String address, String mobile, String email, String note, Integer create_by, Integer update_by, Date create_date, Date update_date) {
        this.id = id;
        this.contact_name = contact_name;
        this.contact_title = contact_title;
        this.address = address;
        this.mobile = mobile;
        this.email = email;
        this.note = note;
        this.create_by = create_by;
        this.update_by = update_by;
        this.create_date = create_date;
        this.update_date = update_date;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContact_name() {
        return this.contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_title() {
        return this.contact_title;
    }

    public void setContact_title(String contact_title) {
        this.contact_title = contact_title;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getCreate_by() {
        return this.create_by;
    }

    public void setCreate_by(Integer create_by) {
        this.create_by = create_by;
    }

    public Integer getUpdate_by() {
        return this.update_by;
    }

    public void setUpdate_by(Integer update_by) {
        this.update_by = update_by;
    }

    public Date getCreate_date() {
        return this.create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return this.update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public CCustomer id(Integer id) {
        setId(id);
        return this;
    }

    public CCustomer contact_name(String contact_name) {
        setContact_name(contact_name);
        return this;
    }

    public CCustomer contact_title(String contact_title) {
        setContact_title(contact_title);
        return this;
    }

    public CCustomer address(String address) {
        setAddress(address);
        return this;
    }

    public CCustomer mobile(String mobile) {
        setMobile(mobile);
        return this;
    }

    public CCustomer email(String email) {
        setEmail(email);
        return this;
    }

    public CCustomer note(String note) {
        setNote(note);
        return this;
    }

    public CCustomer create_by(Integer create_by) {
        setCreate_by(create_by);
        return this;
    }

    public CCustomer update_by(Integer update_by) {
        setUpdate_by(update_by);
        return this;
    }

    public CCustomer create_date(Date create_date) {
        setCreate_date(create_date);
        return this;
    }

    public CCustomer update_date(Date update_date) {
        setUpdate_date(update_date);
        return this;
    }



}
