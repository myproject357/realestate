package com.devcamp.realestate.entity;
import java.util.Date;

import javax.persistence.*;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "master_layout")
public class CMasterLayout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String description;

    private Integer project_id;

    private Double acreage;

    private String apartment_list;

    private String photo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_create", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_update", nullable = true)
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date updateDate;


    public CMasterLayout() {
    }

    public CMasterLayout(int id, String name, String description, Integer project_id, Double acreage, String apartment_list, String photo, Date createDate, Date updateDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.project_id = project_id;
        this.acreage = acreage;
        this.apartment_list = apartment_list;
        this.photo = photo;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getProject_id() {
        return this.project_id;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public Double getAcreage() {
        return this.acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public String getApartment_list() {
        return this.apartment_list;
    }

    public void setApartment_list(String apartment_list) {
        this.apartment_list = apartment_list;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public CMasterLayout id(Integer id) {
        setId(id);
        return this;
    }

    public CMasterLayout name(String name) {
        setName(name);
        return this;
    }

    public CMasterLayout description(String description) {
        setDescription(description);
        return this;
    }

    public CMasterLayout project_id(Integer project_id) {
        setProject_id(project_id);
        return this;
    }

    public CMasterLayout acreage(Double acreage) {
        setAcreage(acreage);
        return this;
    }

    public CMasterLayout apartment_list(String apartment_list) {
        setApartment_list(apartment_list);
        return this;
    }

    public CMasterLayout photo(String photo) {
        setPhoto(photo);
        return this;
    }

    public CMasterLayout createDate(Date createDate) {
        setCreateDate(createDate);
        return this;
    }

    public CMasterLayout updateDate(Date updateDate) {
        setUpdateDate(updateDate);
        return this;
    }


}