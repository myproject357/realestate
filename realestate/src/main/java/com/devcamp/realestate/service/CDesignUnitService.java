package com.devcamp.realestate.service;

import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.CDesignUnit;

@Service
public class CDesignUnitService {
    public void setGetDesignUnitService(CDesignUnit newInvestor, CDesignUnit investor) {
        newInvestor.setName(investor.getName());
        newInvestor.setAddress(investor.getAddress());
        newInvestor.setDescription(investor.getDescription());
        newInvestor.setEmail(investor.getEmail());
        newInvestor.setFax(investor.getFax());
        newInvestor.setNote(investor.getNote());
        newInvestor.setPhone(investor.getPhone());
        newInvestor.setPhone2(investor.getPhone2());
        newInvestor.setProjects(investor.getProjects());
        newInvestor.setWebsite(investor.getWebsite());
    }
}
