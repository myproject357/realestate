package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CRealEstate;
import com.devcamp.realestate.repository.CRealEstateRepository;
import com.devcamp.realestate.service.CRealEstateService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CRealEstateController {
    @Autowired
    CRealEstateRepository pCRealEstateRepository;

    @Autowired
    CRealEstateService pCRealEstateService;
    
    @GetMapping("/all-realestates")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CRealEstate>> getAllRealEstate() {
        try {
            ArrayList<CRealEstate> customers = new ArrayList<>();
            pCRealEstateRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all-realestatesc")
    public ResponseEntity<List<CRealEstate>> getAllRealEstateC() {
        try {
            ArrayList<CRealEstate> customers = new ArrayList<>();
            pCRealEstateRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestate/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") Long id) {
        Optional<CRealEstate> customerData = pCRealEstateRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/realestatelike/{id}")
    public ResponseEntity<Object> getRealEstateLike(@PathVariable("id") Long id) {
        try {
            ArrayList<CRealEstate> customers = new ArrayList<>();
            pCRealEstateRepository.findRealEstateByRequestLike(id).forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestatecomfirm/{comfirm}")
    public ResponseEntity<Object> getRealEstateComfirm(@PathVariable("comfirm") Integer comfirm) {
        try {
            ArrayList<CRealEstate> customers = new ArrayList<>();
            pCRealEstateRepository.findRealEstateByComfirm(comfirm).forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestateprojectid/{project_id}")
    public ResponseEntity<Object> getRealEstateProjectIdAndComfirm(@PathVariable("project_id") Integer project_id ) {
        try {
            ArrayList<CRealEstate> customers = new ArrayList<>();
            pCRealEstateRepository.findRealEstateByProject(project_id).forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestatecreateby/{create_by}")
    public ResponseEntity<Object> getRealEstateLega(@PathVariable("create_by") Integer create_by ) {
        try {
            ArrayList<CRealEstate> customers = new ArrayList<>();
            pCRealEstateRepository.findRealEstateByCreateBy(create_by).forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestaterecommend/{recommend}")
    public ResponseEntity<Object> getRealEstateRecommend(@PathVariable("recommend") Integer recommend ) {
        try {
            ArrayList<CRealEstate> customers = new ArrayList<>();
            pCRealEstateRepository.findRealEstateByRecommend(recommend).forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/realestate/create")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' , 'CUSTOMER' )")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CRealEstate employee) {
        Optional<CRealEstate> customerData = pCRealEstateRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CRealEstate newEmployee = new CRealEstate();
            
            pCRealEstateService.setGetRealEstateService(newEmployee, employee);
            newEmployee.setComfirm(employee.getComfirm());

            CRealEstate saveRole = pCRealEstateRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PostMapping("/realestateme/create")
    public ResponseEntity<Object> createCustomer(@Validated @RequestBody CRealEstate employee) {
        Optional<CRealEstate> customerData = pCRealEstateRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CRealEstate newEmployee = new CRealEstate();
            
            pCRealEstateService.setGetRealEstateService(newEmployee, employee);
            newEmployee.setComfirm(employee.getComfirm());

            CRealEstate saveRole = pCRealEstateRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/realestate/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") Long id, @Validated @RequestBody CRealEstate employee) {
        Optional<CRealEstate> customerData = pCRealEstateRepository.findById(id);
        if (customerData.isPresent()) {
            CRealEstate newEmployee = customerData.get();

            pCRealEstateService.setGetRealEstateService(newEmployee, employee);
            newEmployee.setComfirm(employee.getComfirm());

            CRealEstate saveRole = pCRealEstateRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @PutMapping("/realestate/update/{id}/{userId}")
    public ResponseEntity<Object> updateCustomerbyCreateById(@PathVariable("id") Long id , @PathVariable("userId") Long userId, @Validated @RequestBody CRealEstate employee) {
        Optional<CRealEstate> customerData = pCRealEstateRepository.findById(id);
        if (customerData.isPresent()) {
            CRealEstate newEmployee = customerData.get();

            pCRealEstateService.setGetRealEstateService(newEmployee, employee);

            CRealEstate saveRole = pCRealEstateRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/realestate/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") Long id){
        try{
            pCRealEstateRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }  
}
