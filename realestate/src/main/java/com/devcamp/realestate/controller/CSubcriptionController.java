package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CSubscriptions;
import com.devcamp.realestate.repository.CSubcriptionRepository;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CSubcriptionController {
    @Autowired
    CSubcriptionRepository ơCSubcriptionRepository;

    @GetMapping("/all-subscription")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CSubscriptions>> getAllMsLayout() {
        try {
            ArrayList<CSubscriptions> customers = new ArrayList<>();
            ơCSubcriptionRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/subscription/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CSubscriptions> customerData = ơCSubcriptionRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/subscription/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CSubscriptions employee) {
        Optional<CSubscriptions> customerData = ơCSubcriptionRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CSubscriptions newEmployee = new CSubscriptions();

           newEmployee.setAuthenticationtoken(employee.getAuthenticationtoken());
           newEmployee.setContentencoding(employee.getContentencoding());
           newEmployee.setEndpoint(employee.getEndpoint());
           newEmployee.setPublickey(employee.getPublickey());
           newEmployee.setUser(employee.getUser());

           CSubscriptions saveRole = ơCSubcriptionRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/subscription/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CSubscriptions employee) {
        Optional<CSubscriptions> customerData = ơCSubcriptionRepository.findById(id);
        if (customerData.isPresent()) {
            CSubscriptions newEmployee = customerData.get();

            newEmployee.setAuthenticationtoken(employee.getAuthenticationtoken());
            newEmployee.setContentencoding(employee.getContentencoding());
            newEmployee.setEndpoint(employee.getEndpoint());
            newEmployee.setPublickey(employee.getPublickey());
            newEmployee.setUser(employee.getUser());

            CSubscriptions saveRole = ơCSubcriptionRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/subscription/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            ơCSubcriptionRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }          
}
