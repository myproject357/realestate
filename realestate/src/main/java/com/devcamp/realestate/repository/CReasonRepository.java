package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CReason;

public interface CReasonRepository extends JpaRepository<CReason, Integer>{
    
}
