package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CCustomer;
import com.devcamp.realestate.repository.CCustomerRepository;
import com.devcamp.realestate.service.CCUstomerService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CCustomerController {
    @Autowired 
    CCustomerRepository pCCustomerRepository;

    @Autowired 
    CCUstomerService pCcUstomerService;

    @GetMapping("/all-customers")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CCustomer>> getAllCustomer() {
        try {
            ArrayList<CCustomer> customers = new ArrayList<CCustomer>();
            pCCustomerRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CCustomer> customerData = pCCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/customer/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CCustomer customer) {
        Optional<CCustomer> customerData = pCCustomerRepository.findById(customer.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CCustomer newCustomer = new CCustomer();

            pCcUstomerService.setGetCustomerService(newCustomer, customer);

            CCustomer saveRole = pCCustomerRepository.save(newCustomer);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/customer/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CCustomer customer) {
        Optional<CCustomer> customerData = pCCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            CCustomer newCustomer = customerData.get();

            pCcUstomerService.setGetCustomerService(newCustomer, customer);

            CCustomer saveRole = pCCustomerRepository.save(newCustomer);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/customer/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCCustomerRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
