package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CEmployee;
import com.devcamp.realestate.repository.CEmployeeRepository;
import com.devcamp.realestate.service.CEmployeeService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CEmployeeController {
    @Autowired
    CEmployeeRepository pCEmployeeRepository;

    @Autowired
    CEmployeeService pCEmployeeService;

    @GetMapping("/all-employees")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CEmployee>> getAllEmp() {
        try {
            ArrayList<CEmployee> customers = new ArrayList<CEmployee>();
            pCEmployeeRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CEmployee> customerData = pCEmployeeRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/employee/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CEmployee employee) {
        Optional<CEmployee> customerData = pCEmployeeRepository.findById(employee.getEmployeeID());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CEmployee newEmployee = new CEmployee();

            pCEmployeeService.setGetEmployeeService(newEmployee, employee);

            CEmployee saveRole = pCEmployeeRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/employee/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CEmployee employee) {
        Optional<CEmployee> customerData = pCEmployeeRepository.findById(id);
        if (customerData.isPresent()) {
            CEmployee newEmployee = customerData.get();

            pCEmployeeService.setGetEmployeeService(newEmployee, employee);

            CEmployee saveRole = pCEmployeeRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/employee/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCEmployeeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
