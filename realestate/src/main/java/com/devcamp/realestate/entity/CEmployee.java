package com.devcamp.realestate.entity;

import java.sql.Date;

import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class CEmployee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int EmployeeID;
    @Column(name="last_name")
    private String lastName;
    @Column(name="first_name")
    private String firstName;

    private String Title;
    @Column(name="title_of_courtesy")
    private String titleOfCourtesy;
    @Column(name="birth_date")
    private Date BirthDate; 
    @Column(name="hire_date")
    private Date HireDate;

    private String Address;

    private String City;

    private String Region;
    @Column(name="postal_code")
    private String postalCode;

    private String Country;
    @Column(name="home_phone")
    private String homePhone;

    private String Extension;

    private String Photo;

    private String Notes;
    @Column(name="reports_to")
    private Integer reportsTo;

    private String Username;

    private String Password;

    private String Email;

    private boolean Activated;

    private String Profile;
    @Column(name="user_level")
    private Integer userLevel;

    public CEmployee() {
    }



    public CEmployee(int EmployeeID, String lastName, String firstName, String Title, String titleOfCourtesy, Date BirthDate, Date HireDate, String Address, String City, String Region, String postalCode, String Country, String HomePhone, String Extension, String Photo, String Notes, Integer reportsTo, String Username, String Password, String Email, boolean Activated, String Profile, Integer userLevel) {
        this.EmployeeID = EmployeeID;
        this.lastName = lastName;
        this.firstName = firstName;
        this.Title = Title;
        this.titleOfCourtesy = titleOfCourtesy;
        this.BirthDate = BirthDate;
        this.HireDate = HireDate;
        this.Address = Address;
        this.City = City;
        this.Region = Region;
        this.postalCode = postalCode;
        this.Country = Country;
        this.homePhone = HomePhone;
        this.Extension = Extension;
        this.Photo = Photo;
        this.Notes = Notes;
        this.reportsTo = reportsTo;
        this.Username = Username;
        this.Password = Password;
        this.Email = Email;
        this.Activated = Activated;
        this.Profile = Profile;
        this.userLevel = userLevel;
    }

    public int getEmployeeID() {
        return this.EmployeeID;
    }

    public void setEmployeeID(int EmployeeID) {
        this.EmployeeID = EmployeeID;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getTitle() {
        return this.Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getTitleOfCourtesy() {
        return this.titleOfCourtesy;
    }

    public void setTitleOfCourtesy(String titleOfCourtesy) {
        this.titleOfCourtesy = titleOfCourtesy;
    }

    public Date getBirthDate() {
        return this.BirthDate;
    }

    public void setBirthDate(Date BirthDate) {
        this.BirthDate = BirthDate;
    }

    public Date getHireDate() {
        return this.HireDate;
    }

    public void setHireDate(Date HireDate) {
        this.HireDate = HireDate;
    }

    public String getAddress() {
        return this.Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getCity() {
        return this.City;
    }

    public void setCity(String City) {
        this.City = City;
    }

    public String getRegion() {
        return this.Region;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return this.Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    public String getHomePhone() {
        return this.homePhone;
    }

    public void setHomePhone(String HomePhone) {
        this.homePhone = HomePhone;
    }

    public String getExtension() {
        return this.Extension;
    }

    public void setExtension(String Extension) {
        this.Extension = Extension;
    }

    public String getPhoto() {
        return this.Photo;
    }

    public void setPhoto(String Photo) {
        this.Photo = Photo;
    }

    public String getNotes() {
        return this.Notes;
    }

    public void setNotes(String Notes) {
        this.Notes = Notes;
    }

    public Integer getReportsTo() {
        return this.reportsTo;
    }

    public void setReportsTo(Integer reportsTo) {
        this.reportsTo = reportsTo;
    }

    public String getUsername() {
        return this.Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return this.Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getEmail() {
        return this.Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public boolean isActivated() {
        return this.Activated;
    }

    public boolean getActivated() {
        return this.Activated;
    }

    public void setActivated(boolean Activated) {
        this.Activated = Activated;
    }

    public String getProfile() {
        return this.Profile;
    }

    public void setProfile(String Profile) {
        this.Profile = Profile;
    }

    public Integer getUserLevel() {
        return this.userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public CEmployee EmployeeID(int EmployeeID) {
        setEmployeeID(EmployeeID);
        return this;
    }

    public CEmployee lastName(String lastName) {
        setLastName(lastName);
        return this;
    }

    public CEmployee firstName(String firstName) {
        setFirstName(firstName);
        return this;
    }

    public CEmployee Title(String Title) {
        setTitle(Title);
        return this;
    }

    public CEmployee titleOfCourtesy(String titleOfCourtesy) {
        setTitleOfCourtesy(titleOfCourtesy);
        return this;
    }

    public CEmployee BirthDate(Date BirthDate) {
        setBirthDate(BirthDate);
        return this;
    }

    public CEmployee HireDate(Date HireDate) {
        setHireDate(HireDate);
        return this;
    }

    public CEmployee Address(String Address) {
        setAddress(Address);
        return this;
    }

    public CEmployee City(String City) {
        setCity(City);
        return this;
    }

    public CEmployee Region(String Region) {
        setRegion(Region);
        return this;
    }

    public CEmployee postalCode(String postalCode) {
        setPostalCode(postalCode);
        return this;
    }

    public CEmployee Country(String Country) {
        setCountry(Country);
        return this;
    }

    public CEmployee HomePhone(String HomePhone) {
        setHomePhone(HomePhone);
        return this;
    }

    public CEmployee Extension(String Extension) {
        setExtension(Extension);
        return this;
    }

    public CEmployee Photo(String Photo) {
        setPhoto(Photo);
        return this;
    }

    public CEmployee Notes(String Notes) {
        setNotes(Notes);
        return this;
    }

    public CEmployee reportsTo(Integer reportsTo) {
        setReportsTo(reportsTo);
        return this;
    }

    public CEmployee Username(String Username) {
        setUsername(Username);
        return this;
    }

    public CEmployee Password(String Password) {
        setPassword(Password);
        return this;
    }

    public CEmployee Email(String Email) {
        setEmail(Email);
        return this;
    }

    public CEmployee Activated(boolean Activated) {
        setActivated(Activated);
        return this;
    }

    public CEmployee Profile(String Profile) {
        setProfile(Profile);
        return this;
    }

    public CEmployee userLevel(Integer userLevel) {
        setUserLevel(userLevel);
        return this;
    }



}
