package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.CInvestor;
import com.devcamp.realestate.repository.CInvestorRepository;
import com.devcamp.realestate.service.CInvestorService;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CInvestorController {
    @Autowired 
    CInvestorRepository pCInvestorRepository;

    @Autowired
    CInvestorService pCInvestorService;

    @GetMapping("/all-investors")
    @PreAuthorize("hasAnyRole('ADMIN' , 'HOMESELLER' )")
    public ResponseEntity<List<CInvestor>> getAllInv() {
        try {
            ArrayList<CInvestor> customers = new ArrayList<>();
            pCInvestorRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investor/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id) {
        Optional<CInvestor> customerData = pCInvestorRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/investor/create")
    public ResponseEntity<Object> createCustomerS(@Validated @RequestBody CInvestor employee) {
        Optional<CInvestor> customerData = pCInvestorRepository.findById(employee.getId());
        try {
            if (customerData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Customer already exsit  ");
            }
            CInvestor newEmployee = new CInvestor();

            pCInvestorService.setGetInvestorService(newEmployee, employee);

            CInvestor saveRole = pCInvestorRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/investor/update/{id}")
    public ResponseEntity<Object> updateCustomerS(@PathVariable("id") int id, @Validated @RequestBody CInvestor employee) {
        Optional<CInvestor> customerData = pCInvestorRepository.findById(id);
        if (customerData.isPresent()) {
            CInvestor newEmployee = customerData.get();

            pCInvestorService.setGetInvestorService(newEmployee, employee);

            CInvestor saveRole = pCInvestorRepository.save(newEmployee);
            return new ResponseEntity<>(saveRole, HttpStatus.OK);
        } else {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Customer: " + id);
        }
    }

    @DeleteMapping("/investor/delete/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable("id") int id){
        try{
            pCInvestorRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
}
