package com.devcamp.realestate.service;

import org.springframework.stereotype.Service;

import com.devcamp.realestate.entity.CProject;

@Service
public class CProjectService {
    public void setGetProjectService(CProject newEmployee , CProject employee) {
        newEmployee.setAcreage(employee.getAcreage());
        newEmployee.setAddress(employee.getAddress());
        newEmployee.setApartmentt_area(employee.getApartmentt_area());
        newEmployee.setConstruct_area(employee.getConstruct_area());
        newEmployee.setConstruction_contractor(employee.getConstruction_contractor());
        newEmployee.setDescription(employee.getDescription());
        newEmployee.setDesignUnit(employee.getDesignUnit());
        newEmployee.setInvestor(employee.getInvestor());
        newEmployee.setNum_apartment(employee.getNum_apartment());
        newEmployee.setNum_block(employee.getNum_block());
        newEmployee.setNum_floors(employee.getNum_floors());
        newEmployee.setRegion_link(employee.getRegion_link());
        newEmployee.setSlogan(employee.getSlogan());
        newEmployee.setUtilities(employee.getUtilities());
        newEmployee.set_district_id(employee.get_district_id());
        newEmployee.set_lat(employee.get_lat());
        newEmployee.set_lng(employee.get_lng());
        newEmployee.set_name(employee.get_name());
        newEmployee.set_province_id(employee.get_province_id());
        newEmployee.set_street_id(employee.get_street_id());
        newEmployee.set_ward_id(employee.get_ward_id());
    }
}
