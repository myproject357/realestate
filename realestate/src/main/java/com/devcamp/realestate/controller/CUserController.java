package com.devcamp.realestate.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.devcamp.realestate.entity.User;
import com.devcamp.realestate.repository.UserRepository;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CUserController {
    @Autowired 
    UserRepository pRepository;

    @GetMapping("/all-users")
    public ResponseEntity<List<User>> getAllRealEstate() {
        try {
            ArrayList<User> customers = new ArrayList<>();
            pRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") Long id) {
        Optional<User> customerData = pRepository.findById(id);
        if (customerData.isPresent()) {
            return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
