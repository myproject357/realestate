package com.devcamp.realestate.entity;

import javax.persistence.*;
import javax.persistence.Table;

@Entity
@Table(name = "reason")
public class CReason {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String reason1;
    private String reason2;
    private String reason3;
    private String reason4;
    private String reason5;
    private String reason6;
    private String reason7;
    private String reason8;
    private String reason9;
    private String reason10;

    public CReason() {
    }

    public CReason(Integer id, String reason1, String reason2, String reason3, String reason4, String reason5, String reason6, String reason7, String reason8, String reason9, String reason10) {
        this.id = id;
        this.reason1 = reason1;
        this.reason2 = reason2;
        this.reason3 = reason3;
        this.reason4 = reason4;
        this.reason5 = reason5;
        this.reason6 = reason6;
        this.reason7 = reason7;
        this.reason8 = reason8;
        this.reason9 = reason9;
        this.reason10 = reason10;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReason1() {
        return this.reason1;
    }

    public void setReason1(String reason1) {
        this.reason1 = reason1;
    }

    public String getReason2() {
        return this.reason2;
    }

    public void setReason2(String reason2) {
        this.reason2 = reason2;
    }

    public String getReason3() {
        return this.reason3;
    }

    public void setReason3(String reason3) {
        this.reason3 = reason3;
    }

    public String getReason4() {
        return this.reason4;
    }

    public void setReason4(String reason4) {
        this.reason4 = reason4;
    }

    public String getReason5() {
        return this.reason5;
    }

    public void setReason5(String reason5) {
        this.reason5 = reason5;
    }

    public String getReason6() {
        return this.reason6;
    }

    public void setReason6(String reason6) {
        this.reason6 = reason6;
    }

    public String getReason7() {
        return this.reason7;
    }

    public void setReason7(String reason7) {
        this.reason7 = reason7;
    }

    public String getReason8() {
        return this.reason8;
    }

    public void setReason8(String reason8) {
        this.reason8 = reason8;
    }

    public String getReason9() {
        return this.reason9;
    }

    public void setReason9(String reason9) {
        this.reason9 = reason9;
    }

    public String getReason10() {
        return this.reason10;
    }

    public void setReason10(String reason10) {
        this.reason10 = reason10;
    }

    public CReason id(Integer id) {
        setId(id);
        return this;
    }

    public CReason reason1(String reason1) {
        setReason1(reason1);
        return this;
    }

    public CReason reason2(String reason2) {
        setReason2(reason2);
        return this;
    }

    public CReason reason3(String reason3) {
        setReason3(reason3);
        return this;
    }

    public CReason reason4(String reason4) {
        setReason4(reason4);
        return this;
    }

    public CReason reason5(String reason5) {
        setReason5(reason5);
        return this;
    }

    public CReason reason6(String reason6) {
        setReason6(reason6);
        return this;
    }

    public CReason reason7(String reason7) {
        setReason7(reason7);
        return this;
    }

    public CReason reason8(String reason8) {
        setReason8(reason8);
        return this;
    }

    public CReason reason9(String reason9) {
        setReason9(reason9);
        return this;
    }

    public CReason reason10(String reason10) {
        setReason10(reason10);
        return this;
    }

}
