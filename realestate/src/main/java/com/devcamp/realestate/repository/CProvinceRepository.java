package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.Cprovince;

public interface CProvinceRepository extends JpaRepository<Cprovince,Integer>{
    
}
