package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CCustomer;

public interface CCustomerRepository extends JpaRepository<CCustomer,Integer>{
    
}
