package com.devcamp.realestate.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestate.entity.CConstructionContrator;

public interface CConstructionContratorRepository extends JpaRepository<CConstructionContrator,Integer>{
    
}
