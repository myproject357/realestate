package com.devcamp.realestate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestate.entity.CDistrict;

public interface CDistrictRepository extends JpaRepository<CDistrict,Integer>{
    @Query(value = "SELECT * FROM district WHERE _province_id LIKE :provinceId%", nativeQuery = true)
	List<CDistrict> findDistrictLike(@Param("provinceId") Integer provinceId);
}
